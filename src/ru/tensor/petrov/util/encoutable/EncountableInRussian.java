package ru.tensor.petrov.util.encoutable;

public class EncountableInRussian<T> implements Encountable<T> {

    private final T god, goda, let;

    public EncountableInRussian(T god, T goda, T let) {
        this.god = god;
        this.goda = goda;
        this.let = let;
    }

    @Override public T encount(int cnt) {
        if (cnt < 21)
            return do21(cnt);

        cnt %= 100;
        return cnt < 21
                ? do21(cnt)
                : do21(cnt % 10);
    }

    private T do21(int cnt) {
        switch (cnt) {
            case 1:
                return god;

            case 2:
            case 3:
            case 4:
                return goda;

            default:
                return let;
        }
    }

}