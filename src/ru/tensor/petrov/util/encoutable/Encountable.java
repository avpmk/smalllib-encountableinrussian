package ru.tensor.petrov.util.encoutable;

public interface Encountable<T> {
    T encount(int cnt);
}