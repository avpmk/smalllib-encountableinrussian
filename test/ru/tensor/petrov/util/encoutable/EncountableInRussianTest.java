package ru.tensor.petrov.util.encoutable;

import org.junit.Test;

import static org.junit.Assert.*;
import static ru.tensor.petrov.util.encoutable.EncountableInRussianTest.Yrs.GOD;
import static ru.tensor.petrov.util.encoutable.EncountableInRussianTest.Yrs.GODA;
import static ru.tensor.petrov.util.encoutable.EncountableInRussianTest.Yrs.LET;

public class EncountableInRussianTest {

    enum Yrs { GOD, GODA, LET }

    EncountableInRussian<Yrs> e = new EncountableInRussian<>(GOD, GODA, LET);

    @Test public void testEncount_small() {
        assertEquals(e.encount(0), LET);
        assertEquals(e.encount(1), GOD);
        assertEquals(e.encount(2), GODA);
        assertEquals(e.encount(3), GODA);
        assertEquals(e.encount(4), GODA);
        assertEquals(e.encount(5), LET);
        assertEquals(e.encount(20), LET);
    }

    @Test public void testEncount_middle() {
        assertEquals(e.encount(100), LET);
        assertEquals(e.encount(101), GOD);
        assertEquals(e.encount(102), GODA);
        assertEquals(e.encount(103), GODA);
        assertEquals(e.encount(104), GODA);
        assertEquals(e.encount(105), LET);
        assertEquals(e.encount(120), LET);
    }

    @Test public void testEncount_large1() {
        assertEquals(e.encount(10100), LET);
        assertEquals(e.encount(10101), GOD);
        assertEquals(e.encount(10102), GODA);
        assertEquals(e.encount(10103), GODA);
        assertEquals(e.encount(10104), GODA);
        assertEquals(e.encount(10105), LET);
        assertEquals(e.encount(10120), LET);
    }

    @Test public void testEncount_large2() {
        assertEquals(e.encount(10000), LET);
        assertEquals(e.encount(10001), GOD);
        assertEquals(e.encount(10002), GODA);
        assertEquals(e.encount(10003), GODA);
        assertEquals(e.encount(10004), GODA);
        assertEquals(e.encount(10005), LET);
        assertEquals(e.encount(10020), LET);
    }
}